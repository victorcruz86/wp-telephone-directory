<?php
if (!class_exists('WPTelephoneDirectory_Content')) {
	class WPTelephoneDirectory_Content
	{
		/**
		 * The name of table in the DB
		 * @static
		 * @var string
		 */
		static $table_name = 'content';

		/**
		 * Add item DB
		 *
		 * @static
		 * @param array $data with all the key/values to be added to table
		 * @return array with [0] as int|bool ID generated for an AUTO_INCREMENT column by the most recent INSERT query
		 * 					or FALSE when the query wasn't executed succesfully
		 * 					and [1] as post ID
		 * @access public
		 */
		static public function add($data) {

			self::set_db_obj(); // Create object db
			if ( self::$db_obj != NULL ) {

				// Set current time as added time
				$data['added_datetime'] = date('Y-m-d H:i:s');
				$data['updated_datetime'] = $data['added_datetime'];

				$added_id = self::$db_obj->add($data);

				return $added_id;
			}
			else
				return FALSE;
		}

		/**
		 * Update item DB
		 *
		 * @static
		 * @param array $data with all the key/values to be updated in the table
		 * @param array $where with all the key/values to filter the update
		 * @return bool true when query was executed succesfully, else return FALSE
		 * @access public
		 */
		static public function update($data,$where)
		{
			// Set current time as added time
			$data['updated_datetime'] = date('Y-m-d H:i:s');

			self::set_db_obj();
			if (self::$db_obj!=NULL) {
				// Update Content
				return self::$db_obj->update($data,$where);
			} else
				return FALSE;
		}

		/**
		 * Delete item DB
		 *
		 * @static
		 * @param int $id value of the field of the data to delete
		 * @param string $field name of the column to filter
		 * @param string $field_type type of the column to filter (can be '%s' for strings or '%d' for numeric values)
		 * @return bool true when query was executed succesfully, else return FALSE
		 * @access public
		 */
		static public function delete($id, $field='', $field_type='')
		{
			self::set_db_obj();
			if (self::$db_obj!=NULL) {
				return self::$db_obj->delete($id,$field,$field_type);
			}
			else
				return FALSE;
		}

		/**
		 * Check if already exist a Content with the same name
		 *
		 * @param	string	$name
		 * @static
		 * @return	bool
		 */
		static function already_by_name($name) {
			global $wpdb;

			$query = 'select * from '
				. $wpdb->prefix . WPTelephoneDirectory::$db_prefix
				. self::$table_name
				. " where name='$name'";

			$result = $wpdb->get_row($query);
			if ($result) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}

		/*
		 * except "SuperContentInsert" that is the name of the class of the plugin
		 * Begin Common Code
		 * This can be avoid since PHP 5.3 (http://www.php.net/manual/en/language.oop5.late-static-bindings.php)
		 */

		/**
		 * The Object to access DB
		 * @static
		 * @var GpcKits_DBO
		 */
		static $db_obj;

		/**
		 * Set DB Object
		 *
		 * @static
		 * @global $wpdb used to access to WordPress Object that manage DB
		 * @param string $table optional value of the table
		 * @access public
		 */
		static public function set_db_obj($table='')
		{
			global $wpdb;
			if ($table=='') $table = self::$table_name;
			$full_table_name =  $wpdb->prefix . WPTelephoneDirectory::$db_prefix . $table;

			if (class_exists('WPTelephoneDirectory_DBO'))
				self::$db_obj = new WPTelephoneDirectory_DBO($full_table_name);
		}

		/**
		 * Get all items from DB
		 *
		 * @static
		 * @param int $id value of the field to filter
		 * @param string $field name of the column to filter
		 * @param string $order_by name of the column to order by
		 * @param string $field_type type of the column to filter (can be '%s' for strings or '%d' for numeric values)
		 * @return array
		 * @access public
		 */
		static public function get_all($id='', $field='', $order_by='', $field_type='')
		{
			self::set_db_obj();
			if (self::$db_obj!=NULL)
				return self::$db_obj->get_all($id, $field, $order_by, $field_type);
			else
				return array();
		}

		/**
		 * Get item from DB
		 *
		 * @static
		 * @param int $id value of the field to filter
		 * @param string $field name of the column to filter
		 * @param string $field_type type of the column to filter (can be '%s' for strings or '%d' for numeric values)
		 * @return array|NULL data when query was executed succesfully, else return NULL
		 * @access public
		 */
		static public function get($id, $field='', $field_type='')
		{
			self::set_db_obj();
			if (self::$db_obj!=NULL)
				return self::$db_obj->get($id, $field, $field_type);
			else
				return NULL;
		}

		/*
		 * End Common Code
		 * This can be avoid since PHP 5.3 (http://www.php.net/manual/en/language.oop5.late-static-bindings.php)
		 */
	}
}