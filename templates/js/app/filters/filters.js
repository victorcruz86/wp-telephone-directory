/**
 * Module Filters specific to app.
 */
'use strict';

angular.module("sclinic.filters", [])

/**
 * Parse content to html.
 */
    .filter('cfhtml', ['$sce', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    }])