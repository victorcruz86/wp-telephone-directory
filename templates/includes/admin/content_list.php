<?php
/**
 * Template to list all the added content blocks.
 *
 * @uses    string        $link_to_add_content_page                                Link to add content
 *
 * @uses    array        $super_content_insert_getall_list_content                List of Content
 * @uses    array        $super_content_insert_getall_list_content->id
 * @uses    array        $super_content_insert_getall_list_content->name
 * @uses    array        $super_content_insert_getall_list_content->shown_in
 * @uses    array        $super_content_insert_getall_list_content->not_shown_in
 * @uses    array        $super_content_insert_getall_list_content->position
 * @uses    array        $super_content_insert_getall_list_content->shortcode
 *
 */
?>

<div class="wrap cf-page">
	<div class="icon32" id="icon-options-general"><br /></div>
	<h2 class="cf-page-header">
		<?php _e('Content Blocks List', WPTelephoneDirectory::$i18n_prefix)?>
		<a class="add-new-h2" href="<?php echo $link_to_add_edit_content_page; ?>"><?php _e('Add New Content', WPTelephoneDirectory::$i18n_prefix)?></a>
		<br />
		<span class="description"><?php _e('This page allows you to see the list of already added content blocks.', WPTelephoneDirectory::$i18n_prefix)?></span>
	</h2>

	<div id="cf-message-container">
		<?php include( WPTelephoneDirectory::$template_dir . '/includes/msg.php' ); ?>
	</div>

	<input type="hidden" id="content_to_delete" name="content_to_delete" value="" />
	<form action="" method="post">
		<table id="content-blocks-list" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th class="column_name"><?php _e('Name', WPTelephoneDirectory::$i18n_prefix)?>&nbsp;&nbsp;</th>
					<th class="column_shown_in"><?php _e('Shown In',WPTelephoneDirectory::$i18n_prefix)?>&nbsp;&nbsp;</th>
					<th class="column_not_shown_in"><?php _e('Not Shown In', WPTelephoneDirectory::$i18n_prefix)?>&nbsp;&nbsp;</th>
					<th class="column_position"><?php _e('Position', WPTelephoneDirectory::$i18n_prefix)?>&nbsp;&nbsp;</th>
					<th class="column_shortcode text-align-center"><?php _e('Shortcode', WPTelephoneDirectory::$i18n_prefix)?>&nbsp;&nbsp;</th>
					<th class="column_actions text-align-center"><?php _e('Actions', WPTelephoneDirectory::$i18n_prefix)?>&nbsp;&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($super_content_insert_getall_list_content as $one_content): ?>
					<tr id="<?php echo $one_content['id'] ?>">
						<td><?php echo $one_content['name']; ?></td>
						<td><?php echo $one_content['shown_in']; ?></td>
						<td><?php echo $one_content['not_shown_in']; ?></td>
						<td><?php echo $one_content['position']; ?></td>
						<td class="text-align-center"><?php echo $one_content['shortcode']; ?></td>
						<td class="actions-column">
							<a class="button-primary" href="<?php echo $link_to_add_edit_content_page; ?>&amp;is_editing=1&amp;id=<?php echo $one_content['id'] ?>"><?php _e('Edit', WPTelephoneDirectory::$i18n_prefix)?></a>
							<a onclick="return false;" class="thickbox button-secondary" title="Confirmation" href="#TB_inline&height=105&width=350&inlineId=thickbox-dialog"><?php _e('Delete', WPTelephoneDirectory::$i18n_prefix)?></a>
						</td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</form>

	<div id="cf-templates-container" class="display-none">
		<div class="cf-error-message ui-state-error ui-corner-all cf-negative cf-position-relative" style="padding: 0 0.7em;">
			<p>
				<span class="cf-icon-suggestion-type"></span>
				<span class="cf-msg-mark"></span>
			</p>
		</div>
		<div class="cf-notification-message ui-state-highlight ui-corner-all cf-positive cf-position-relative" style="padding: 0 0.7em;">
			<span class="cf-icon-suggestion-type"></span>
			<p>
				<span class="cf-msg-mark"></span>
			</p>
		</div>
	</div>

	<div id="thickbox-dialog" style="display:none;">
		<div>
			<p>
				<br />
				Are you sure you want to <strong>DELETE</strong> this content block?
			</p>

			<p class="text-align-center">
				<button class="button-primary">Yes, Delete it</button>
				&nbsp;
				<button class="button-secondary">Cancel</button>
				<span class="cf-ajax-loader-container">
					<img src="<?php echo get_bloginfo ('wpurl') . '/wp-admin/images/wpspin_light.gif'; ?>" class="cf-ajax-loader" alt="Loading..." title="Loading data...">
				</span>
			</p>
		</div>
	</div>
</div>