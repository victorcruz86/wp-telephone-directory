<?php
/**
 * Page to handle all Ajax requests for add data.
 * All data will be returned using JSON format
 *
 * General variables:
 *
 * @uses	$_REQUEST['object']			required
 */

/** Loads the WordPress Environment */
require_once( dirname(__FILE__) . '/../../../../../wp-load.php' );

// Adding
if ( isset($_REQUEST['action']) AND 'del' == $_REQUEST['action'] AND isset($_REQUEST['id']) AND '' != WPTelephoneDirectory_Validator::parse_string( $_REQUEST['id']) AND isset( $_REQUEST['submit_delete'] ) ) {
	//check_admin_referer('SuperContentInserter-add-update-content');

	if ( $data_to_delete = WPTelephoneDirectory_Content::get(WPTelephoneDirectory_Validator::parse_int($_REQUEST['id']) ) ){
		if ( WPTelephoneDirectory_Content::delete($data_to_delete->id) ){
			$message_to_return = __('Deleted successfully.','super-content-insert');
			$type_to_return = 'notification';
		} else {
			$message_to_return = __('Error to insert into database.','super-content-insert');
			$type_to_return = 'error';
		}
	} else {
		$message_to_return = __('Content block not found.','super-content-insert');
		$type_to_return = 'error';
	}
}

$data['message']	= $message_to_return;
$data['type'] 		= $type_to_return;

$json = json_encode($data);
echo $json;
die();