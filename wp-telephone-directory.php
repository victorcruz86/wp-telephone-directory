<?php
/*
Plugin Name: WP Telephone Directory
Version: 0.2
Description: Add and display Content into blog content in specific sections.
*/

/**
 * WordPress Version.
 * @global	string	$wp_version
 */
global $wp_version;

// Avoid name collisions.
if (!class_exists('WPTelephoneDirectory')) {
	class WPTelephoneDirectory
	{
		/**
		 * The url to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_url;

		/**
		 * The path to the plugin
		 *
		 * @static
		 * @var string
		 */
		static $plugin_dir;

		/**
		 * The path to the plugin templates files
		 *
		 * @static
		 * @var string
		 */
		static $template_dir;

		/**
		 * The prefix for the plugin tables in database
		 *
		 * @static
		 * @var string
		 */
		static $db_prefix;

		/**
		 * The prefix for the plugin internationalization
		 *
		 * @static
		 * @var string
		 */
		static $i18n_prefix;

		/**
		 * Array of the Contents
		 *
		 * @static
		 * @var content
		 */
		private $content;

		/**
		 * Executes all initialization code for the plugin.
		 *
		 * @access public
		 */
		function __construct() {

			// Define static values
			self::$plugin_url = trailingslashit( WP_PLUGIN_URL.'/'. dirname( plugin_basename(__FILE__) ));
			self::$plugin_dir = trailingslashit( WP_PLUGIN_DIR.'/'. dirname( plugin_basename(__FILE__) ));
			self::$template_dir = self::$plugin_dir . '/templates';
			self::$db_prefix = 'wp_telephone_directory_';
			self::$i18n_prefix = 'wp_telephone_directory';

			// Include all classes
			include(self::$plugin_dir . '/includes/all_classes.php');

			// Add AJAX support through native WP admin-ajax action.
			add_action('wp_ajax_add', array(&$this, 'ajax_add'));
			add_action('wp_ajax_del', array(&$this, 'ajax_del'));

			// Add Menu Box in Admin Panel
			add_action('admin_menu', array(&$this, 'admin_menu'));

			add_action('init', array( &$this, 'register_shortcode' ) );

//			add_action('init', array(&$this, 'wp_telephone_directory_display_content'), 100);

			// Update box suggestions data on rich text editor changes.
			add_filter('tiny_mce_before_init', array(&$this, 'update_rte'));
		}

		/**
		 * Handles the AJAX action for add objects.
		 *
		 */
		function ajax_add() {
			// This is how you get access to the database.
			global $wpdb;

			include(self::$plugin_dir . '/pages/ajax/add_data.php');
		}

		/**
		 * Handles the AJAX action for delete objects.
		 *
		 */
		function ajax_del() {
			// This is how you get access to the database.
			global $wpdb;

			include(self::$plugin_dir . '/pages/ajax/delete_data.php');
		}

		/**
		 * Update textarea on rich text editor changes.
		 */
		function update_rte($initArray){
			$initArray['setup'] = <<<JS
[function(ed) {
    ed.onChange.add(function(ed, e) {
		// Update HTML view textarea (that is the one used to send the data to server).
		ed.save();
	});
}][0]
JS;

			return $initArray;
		}

		function register_shortcode(){
			add_shortcode( 'sci' , array( &$this, 'display_shortcode'), -1000);
		}

		function display_shortcode($atts){

			// get content by id and chequed if it is shown
			if ( $content = WPTelephoneDirectory_Content::get( $atts['id'] ) and '1' == $content->is_active ){
				if ( '1' == $content->content_is_php ){
					ob_start();
					eval($content->content);
					$eval_result = ob_get_contents();
					ob_end_clean();
					$value =  do_shortcode( $eval_result );
				} else {
					$value = do_shortcode( $content->content );
				}
				return $value;
			}
		}

		/**
		 * Hooks the add of the main menu
		 *
		 * @return void
		 * @access public
		 */
		function admin_menu() {
			add_menu_page(__( 'WP Telephone Directory', self::$i18n_prefix ), __( 'WP Telephone Directory', self::$i18n_prefix ), 'manage_options', basename(__FILE__), array(&$this, 'handle_list_content'),self::$plugin_url . '/templates/images/content_block.png');
			$page_add_content  = add_submenu_page( basename(__FILE__), __( 'Telephone Directory List', self::$i18n_prefix ), __('Content List', self::$i18n_prefix ), 'manage_options', basename(__FILE__) , array(&$this, 'handle_list_content'));
			$page_list_content = add_submenu_page( basename(__FILE__), __( 'Add/Edit Telephone Directory', self::$i18n_prefix ), __('Add/Edit Content', self::$i18n_prefix), 'manage_options',  'super-content-inserter-add-edit.php', array(&$this, 'handle_add_content'));
			$page_help		   = add_submenu_page( basename(__FILE__), __( 'Help', self::$i18n_prefix ), __('Help', self::$i18n_prefix), 'manage_options',  'super-content-inserter-help.php', array(&$this, 'handle_help'));

			add_action( "admin_print_scripts-$page_add_content", array(&$this, 'handle_admin_scripts') );
			add_action( "admin_print_scripts-$page_list_content", array(&$this, 'handle_admin_scripts') );
			add_action( "admin_print_scripts-$page_help", array(&$this, 'handle_admin_scripts') );
		}

		/**
		 * Handles the js variables for handle_admin_scripts
		 *
		 */
		function handle_admin_scripts() {
			wp_localize_script(
				'jquery'
				, 'SCI'
				, array(
					'plugin_url' => self::$plugin_url
				)
			);
		}

		/**
		 * Handles the main menu for handle_list_content
		 *
		 * Used for: list Contents
		 *
		 * @return void
		 * @access public
		 */
		function handle_list_content() {
			// Load needed styles.
			wp_enqueue_style('thickbox');

			wp_enqueue_style('twitter-bootstrap', self::$plugin_url . 'templates/js/lib/bootstrap/css/bootstrap.css');
			wp_enqueue_style('sci-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/css/DT_bootstrap.css');

			wp_enqueue_style('sci-global', self::$plugin_url . 'templates/css/global.css');

			// Load needed script libraries.
			wp_enqueue_script('thickbox');
			wp_enqueue_script('jquery-effects-highlight');
			wp_enqueue_script('jquery-scrollTo', self::$plugin_url . 'templates/js/lib/jquery.scrollTo.js');
			wp_enqueue_script('jquery-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/js/jquery.dataTables.min.js');
			wp_enqueue_script('jquery-dataTables-bootstrap', self::$plugin_url . 'templates/js/lib/dataTables/js/DT_bootstrap.js');
			wp_enqueue_script('sci-add-edit-content', self::$plugin_url . 'templates/js/list.js');

			include(self::$plugin_dir . '/includes/admin/handle_list_content.php');
		}

		/**
		 * Handles the main menu for handle_add_content
		 *
		 * Used for: add/edit a Content
		 *
		 * @return void
		 * @access public
		 */
		function handle_add_content() {
			// Select the Text editor by default.
			add_filter( 'wp_default_editor', create_function('', 'return "html";') );

			// Load needed styles.
			wp_enqueue_style('jquery-chosen', self::$plugin_url . 'templates/js/lib/chosen/chosen.css');
			wp_enqueue_style('jquery-iCheckbox', self::$plugin_url . 'templates/js/lib/iCheckbox/css/iCheckbox.css');
			wp_enqueue_style('sci-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/css/jquery.dataTables.css');
			wp_enqueue_style('sci-global', self::$plugin_url . 'templates/css/global.css');

			// Load needed script libraries.
			wp_enqueue_script('jquery-effects-highlight');
			wp_enqueue_script('jquery-scrollTo', self::$plugin_url . 'templates/js/lib/jquery.scrollTo.js');
			wp_enqueue_script('jquery-form', self::$plugin_url . 'templates/js/lib/jquery.form.js');
			wp_enqueue_script('jquery-chosen', self::$plugin_url . 'templates/js/lib/chosen/chosen.js');
			wp_enqueue_script('jquery-iCheckbox', self::$plugin_url . 'templates/js/lib/iCheckbox/jquery.iCheckbox.js');
			wp_enqueue_script('jquery-dataTables', self::$plugin_url . 'templates/js/lib/dataTables/js/jquery.dataTables.min.js');
			wp_enqueue_script('sci-add-edit-content', self::$plugin_url . 'templates/js/add-edit.js');

			include(self::$plugin_dir . '/includes/admin/handle_add_content.php');
		}

		/**
		 * Handles the main menu for handle_help.
		 *
		 * Used for: Help.
		 *
		 * @return void
		 * @access public
		 */
		function handle_help() {
			wp_enqueue_style('sci-global', self::$plugin_url . 'templates/css/global.css');

			include(self::$plugin_dir . '/includes/admin/handle_help.php');
		}


		function install() {
			// Creating tables
			include(self::$plugin_dir . '/db/tables.php');
		}

		/**
		 * Execute code in deactivation
		 *
		 * @return 	void
		 * @access 	public
		 */
		function uninstall() {
			// TODO: DROP DB
		}
	}
}

$exit_msg = __('This plugin require WordPress 3.1 or newer',WPTelephoneDirectory::$i18n_prefix).'. <a href="http://codex.wordpress.org/Upgrading_WordPress">'.__('Please update',WPTelephoneDirectory::$i18n_prefix).'</a>';
if (version_compare($wp_version, "3.1", "<")) {
	exit($exit_msg);
}

// create new instance of the class
$WPTelephoneDirectory = new WPTelephoneDirectory();
if (isset($WPTelephoneDirectory)) {
	// register the activation-deactivation function by passing the reference to our instance
	register_activation_hook(__FILE__, array(&$WPTelephoneDirectory, 'install'));
	register_deactivation_hook(__FILE__, array(&$WPTelephoneDirectory, 'uninstall'));
}