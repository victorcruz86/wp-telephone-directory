=== Wp Telephone directory ===
Contributors: Victor Cruz
Tags: Telephone
Tested up to: 3.5
Requires at least: 3.5
Stable tag: trunk

== Description ==

Add and display Content into blog content in specific sections.

== Functionalities ==

Add custom content blocks into several areas of blog Posts or Pages.
These content blocks can be from pure text, HTML, banners or PHP code.

== Screenshots ==

1. The "Content Blocks List" page.

== Installation ==

== How PHP content works ==
