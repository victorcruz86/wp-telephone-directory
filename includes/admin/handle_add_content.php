<?php
/**
 * Include to add Content
 *
 * @package admin-panel
 *
 */

// Security: Check if is admin user
WPTelephoneDirectory_Users::an_admin_must_be_authenticated();

global $msg_notify, $msg_error, $wpdb;

$is_editing = 0;

if ( isset( $_GET['is_editing'] ) AND '' != WPTelephoneDirectory_Validator::parse_int( $_GET['is_editing'] ) AND
	 isset( $_GET['id'] ) AND '' != WPTelephoneDirectory_Validator::parse_int( $_GET['id'] )) {

	$content = WPTelephoneDirectory_Content::get( WPTelephoneDirectory_Validator::parse_int( $_GET['id'] ) );

	if ( isset( $content ) ){
		$data['name'] 				= $content->name;
		$data['content_text'] 			= $content->content;
		$data['shortcode'] 			= $content->shortcode;
		$data['content_is_php'] 	= $content->content_is_php;
		$data['top_right_width'] 	= $content->top_right_width;
		$data['top_left_width'] 	= $content->top_left_width;

		// Positions, separate intro db by ';' character
		if ( isset( $content->position ) AND '' != $content->position ){
			$position = explode(';', $content->position);
			( in_array( 'init', $position ) )? 							$data['position_init'] 					= 1 : $data['position_init'] 	= 0;
			( in_array( 'header', $position ) )?				$data['position_header']				= 1 : $data['position_header'] 	= 0;
			( in_array( 'after_post_entry', $position ) )? 	$data['position_after_post_entry'] 		= 1 : $data['position_after_post_entry'] = 0;
			( in_array( 'before_post_entry', $position ) )?	$data['position_before_post_entry'] 	= 1 : $data['position_before_post_entry'] = 0;
			( in_array( 'post_entry_top_right', $position ) )?	$data['position_post_entry_top_right']	= 1 : $data['position_post_entry_top_right'] = 0;
			( in_array( 'post_entry_top_left', $position ) )?	$data['position_post_entry_top_left']	= 1 : $data['position_post_entry_top_left'] = 0;
			( in_array( 'top_posts_sequence', $position ) )?	$data['position_top_posts_sequence']	= 1 : $data['position_top_posts_sequence'] = 0;
			( in_array( 'bottom_posts_sequence', $position ) )?$data['position_bottom_posts_sequence']	= 1 : $data['position_bottom_posts_sequence'] = 0;
			( in_array( 'footer', $position ) )?				$data['position_footer']				= 1 : $data['position_footer'] = 0;
		}

		// Shown in
		if ( isset( $content->shown_in ) AND '' != $content->shown_in ){
			// Explode by convert string intro array with ';' character separator.
			$shown_in_arr =  explode(';', $content->shown_in );
			$shown_in_split = array(); // Output array [home],[pages,8,0,9],[posts,all_posts] ...

			// In each $shown_arr array there are list to the other content, with '::' character separator.
			foreach ( $shown_in_arr as $shown_arr_item ){
				$shown_arr_multi = explode('::', $shown_arr_item );
				$shown_in_split[ $shown_arr_multi[0] ] = $shown_arr_multi;
			}

			( in_array( 'all', $shown_in_arr) )? 	$data['show_in_all'] 				= 1 : $data['show_in_all'] = 0  ;
			( in_array( 'home', $shown_in_arr) )? 	$data['show_in_home'] 				= 1 : $data['show_in_home'] = 0  ;

			if ( isset( $shown_in_split[ 'pages' ] ) )
				$data['show_in_specific_pages'] = 1;
			if ( isset( $shown_in_split[ 'pages' ] ) AND 'all_pages' != $shown_in_split['pages'] )
					$data['show_in_specific_pages_selected'] = $shown_in_split['pages'];
			else
				$data['show_in_specific_pages'] = 0  ;

			if ( isset( $shown_in_split[ 'posts' ] ) )
				$data['show_in_specific_posts'] = 1;
			if ( isset( $shown_in_split[ 'posts' ] ) AND 'all_posts' !=$shown_in_split['posts'])
				$data['show_in_specific_posts_selected'] = $shown_in_split['posts'];
			else
				$data['show_in_specific_posts'] = 0  ;

			if ( isset( $shown_in_split[ 'categories' ] ) )
				$data['show_in_specific_categories'] = 1;
			if ( isset( $shown_in_split[ 'categories' ] ) AND 'all_categories' !=$shown_in_split['categories'])
				$data['show_in_specific_categories_selected'] = $shown_in_split['categories'];
			else
				$data['show_in_specific_categories'] = 0  ;

			if ( isset( $shown_in_split[ 'tags' ] ) )
				$data['show_in_specific_tags'] = 1;
			if ( isset( $shown_in_split[ 'tags' ] ) AND 'all_tags' !=$shown_in_split['tags'])
				$data['show_in_specific_tags_selected'] = $shown_in_split['tags'];
			else
				$data['show_in_specific_tags'] = 0  ;

			( in_array( 'archives', $shown_in_arr) )? 		$data['show_in_archives'] 		= 1 : $data['show_in_archives'] = 0  ;

			if ( isset( $shown_in_split[ 'category_page' ] ) )
				$data['show_in_category_page'] = 1;
			if ( isset( $shown_in_split[ 'category_page' ] ) AND 'all_category_page' != $shown_in_split['category_page'] )
				$data['show_in_specific_categories_page_selected'] = $shown_in_split['category_page'];
			else
				$data['show_in_category_page'] = 0  ;

			if ( isset( $shown_in_split[ 'tags_page' ] ) )
				$data['show_in_tags_page'] = 1;
			if ( isset( $shown_in_split[ 'tags_page' ] ) AND 'all_tags_page' != $shown_in_split['tags_page'])
				$data['show_in_specific_tags_page_selected'] = $shown_in_split['tags_page'];
			else
				$data['show_in_tags_page'] = 0  ;

			( in_array( '404_page', $shown_in_arr) )? 		$data['show_in_404_page'] 		= 1 : $data['show_in_404_page'] = 0  ;


		}

		// Not shown in
		if ( isset( $content->not_shown_in ) AND '' != $content->shown_in ){
			// Explode by convert string intro array with ';' character separator.
			$not_shown_in_arr =  explode(';', $content->not_shown_in );
			$not_shown_in_split = array(); // Output array [home],[pages,8,0,9],[posts,all_posts] ...

			// In each $shown_arr array there are list to the other content, with '::' character separator.
			foreach ( $not_shown_in_arr as $not_shown_arr_item ){
				$not_shown_arr_multi = explode('::', $not_shown_arr_item );
				$not_shown_in_split[ $not_shown_arr_multi[0] ] = $not_shown_arr_multi;
			}

			( in_array( 'all', $not_shown_in_arr) )? 	$data['not_show_in_all'] 				= 1 : $data['not_show_in_all'] = 0  ;
			( in_array( 'home', $not_shown_in_arr) )? 	$data['not_show_in_home'] 				= 1 : $data['not_show_in_home'] = 0  ;

			if ( isset( $not_shown_in_split[ 'pages' ] ) )
				$data['not_show_in_specific_pages'] = 1;
			if ( isset( $not_shown_in_split[ 'pages' ] )  AND 'all_pages' != $not_shown_in_split['pages'])
				$data['not_show_in_specific_pages_selected'] = $not_shown_in_split['pages'];
			else
				$data['not_show_in_specific_pages'] = 0  ;

			if ( isset( $not_shown_in_split[ 'posts' ] ) )
				$data['not_show_in_specific_posts'] = 1;
			if ( isset( $not_shown_in_split[ 'posts' ] ) AND 'all_posts' !=$not_shown_in_split['posts'])
				$data['not_show_in_specific_posts_selected'] = $not_shown_in_split['posts'];
			else
				$data['not_show_in_specific_posts'] = 0  ;

			if ( isset( $not_shown_in_split[ 'categories' ] ) )
				$data['not_show_in_specific_categories'] = 1;
			if ( isset( $not_shown_in_split[ 'categories' ] ) AND 'all_categories' != $not_shown_in_split['categories'])
				$data['not_show_in_specific_categories_selected'] = $not_shown_in_split['categories'];
			else
				$data['not_show_in_specific_categories'] = 0  ;

			if ( isset( $not_shown_in_split[ 'tags' ] ) )
				$data['not_show_in_specific_tags'] = 1;
			if (  isset( $not_shown_in_split[ 'tags' ] ) AND 'all_tags' !=$not_shown_in_split['tags'])
				$data['not_show_in_specific_tags_selected'] = $not_shown_in_split['tags'];
			else
				$data['not_show_in_specific_tags'] = 0  ;

			( in_array( 'archives', $not_shown_in_arr) )? 		$data['not_show_in_archives'] 		= 1 : $data['not_show_in_archives'] = 0  ;

			if ( isset( $not_shown_in_split[ 'category_page' ] ) )
				$data['not_show_in_category_page'] = 1;
			if ( isset( $not_shown_in_split[ 'category_page' ] ) AND 'all_category_page' != $not_shown_in_split['category_page'] )
				$data['not_show_in_specific_categories_page_selected'] = $not_shown_in_split['category_page'];
			else
				$data['not_show_in_category_page'] = 0  ;

			if ( isset( $not_shown_in_split[ 'tags_page' ] ) )
				$data['not_show_in_tags_page'] = 1;
			if ( isset( $not_shown_in_split[ 'tags_page' ] ) AND 'all_tags_page' != $not_shown_in_split['tags_page'])
				$data['not_show_in_specific_tags_page_selected'] = $not_shown_in_split['tags_page'];
			else
				$data['not_show_in_tags_page'] = 0  ;

			( in_array( '404_page', $not_shown_in_arr) )? 		$data['not_show_in_404_page'] 		= 1 : $data['not_show_in_404_page'] = 0  ;


		}
		// In this case is Editing page
		$is_editing = 1;
		$content_id = $content->id;
	}

	if ( isset( $_GET['added']) ){
		$msg_notify[] = 'The content block was added successfully.';
	}

}

// TODO: Create wp-post.class.php and set all query
//get all pages
$all_pages = $wpdb->get_results( "
						SELECT ID, post_title
						FROM $wpdb->posts
						WHERE post_type = 'page'
						AND post_status = 'publish'"
);

//get all posts
$all_posts = $wpdb->get_results( "
						SELECT ID, post_title
						FROM $wpdb->posts
						WHERE post_type = 'post'
						AND post_status = 'publish'"
);

//get all tags
$all_tags = $wpdb->get_results( "
						SELECT $wpdb->terms.term_id, $wpdb->terms.name, $wpdb->terms.slug
						FROM $wpdb->terms
						INNER JOIN $wpdb->term_taxonomy ON ($wpdb->terms.term_id = $wpdb->term_taxonomy.term_id)
						WHERE $wpdb->term_taxonomy.taxonomy = 'post_tag'");

//get all categories
$all_categories = $wpdb->get_results( "
						SELECT $wpdb->terms.term_id, $wpdb->terms.name, $wpdb->terms.slug
						FROM $wpdb->terms
						INNER JOIN $wpdb->term_taxonomy ON ($wpdb->terms.term_id = $wpdb->term_taxonomy.term_id)
						WHERE $wpdb->term_taxonomy.taxonomy = 'category'"
);

// Link to Main Page
$link_to_list_content_page = admin_url('admin.php') . '?page=' . 'super-content-inserter.php';

include( WPTelephoneDirectory::$template_dir . '/includes/admin/add_edit_content.php');
